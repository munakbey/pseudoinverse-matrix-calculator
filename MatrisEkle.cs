﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using  System.Windows.Forms;

namespace TERSMATRİS
{
    public partial class MatrisEkle : Form
    {

        

        

        public static ArrayList textBoxes = new ArrayList();
        ArrayList textBoxDegerler = new ArrayList();
        Secim secim = new Secim();
        public static double determinant;
        public double[,] matrix = new double[Secim.t1, Secim.t2];
        public static double[,] carpımSonuc = new double[Secim.t1, Secim.t1];
        public static double[,] carpımSonuc2 = new double[Secim.t2, Secim.t2];
        public static double[,] SonCarpım = new double[Secim.t2, Secim.t1];
        public ArrayList arrayMin=new ArrayList();
        public static ArrayList minör = new ArrayList();
        public static ArrayList Kofaktör = new ArrayList();//-1 üssü ile çarpılmış hali
        public static double[,] TersMatris;
        public static double[,] transpoze = new double[Secim.t2, Secim.t1];
        double[,] TersMatrisDeğer=new double[1,1];//satır=1 olduğunda kullanılır

        public MatrisEkle()
        {
            InitializeComponent();
            olustur();
           
        }
        public MatrisEkle(int t1, int t2)
        {
            InitializeComponent();
            Secim.t1 = t1;
            Secim.t2 = t2;
            olustur();

            Random random = new Random();
            foreach (TextBox textBox in MatrisEkle.textBoxes)
            {
                if (textBox is TextBox)
                {
                    ((TextBox)textBox).Text = Math.Round((random.NextDouble() * 10), 1).ToString();

                }

            }

        }

        private void MatrisEkle_Load(object sender, EventArgs e)
        {
            
          
        }
      

        public void olustur()

        {
            Secim scm = new Secim();
            for (int i = 0; i < Secim.t1; i++)
                {
                    for (int j = 0; j <Secim.t2; j++)
                    {
                        TextBox textbox = new TextBox();
                        textbox.Location = new System.Drawing.Point(70 + (80 * j), 100 + (25 * i));
                        textbox.Size = new System.Drawing.Size(70, 90);
                        this.Controls.Add(textbox);
                        textBoxes.Add(textbox);
                    }
              
                }
          
            
        }
        public double[,] matriscevir(ArrayList array)
        {
            int k = 0;
            
            for (int i = 0; i <Secim.t1; i++)
            {
                for (int j= 0; j <Secim.t2; j++)
                {
                    
                     matrix[i, j] = Convert.ToDouble(array[k++]);
                    

                }
            }
           
            return matrix;
        }
        public double[,] matriscevir2(ArrayList array,int n)
        {
            ArrayList tersarray = new ArrayList();
            int k = 0;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j <n; j++)
                {
                    
                    matrix[i, j] = Convert.ToDouble(array[k++]);
                  

                }
            }

            return matrix;
        }

        private void button2_Click(object sender, EventArgs e)
        {
           
            foreach (TextBox textBox in textBoxes)
            {
                string value = textBox.Text;
                textBoxDegerler.Add(value);
                
                }
         
             matriscevir(textBoxDegerler);
             Transpoze(matrix);

            CarpımTranspoze(matrix, transpoze);

            if (Secim.t1 > Secim.t2 && determinantAl(carpımSonuc2, Secim.t2) != 0) {
                
            
                    ekMatris(carpımSonuc2, Secim.t2);
                    getTersMatris(Kofaktör, Secim.t2, carpımSonuc2);
                    MatrisCarpim(getTersMatris(Kofaktör, Secim.t2, carpımSonuc2), transpoze, Secim.t2, Secim.t1, Secim.t2);

              
                }
         
            else if(Secim.t1 < Secim.t2 && determinantAl(carpımSonuc,Secim.t1)!=0)
            {

                ekMatris(carpımSonuc, Secim.t1);
                
                   
                  /*  if (Secim.t1 == 1 )
                    {
                        MessageBox.Show(TersMatrisDeğer[0, 0].ToString());
                        MatrisCarpim(transpoze, TersMatrisDeğer, Secim.t2, Secim.t1, Secim.t1);
                    }
                    else
                    {*/
                       // MessageBox.Show("satır bir değil");
                        MatrisCarpim(transpoze, getTersMatris(Kofaktör, Secim.t1, carpımSonuc), Secim.t2, Secim.t1, Secim.t1);
                   // }
                
               
            }
            else
            {
                MessageBox.Show("MATRİSİN DETERMİNANTI SIFIR OLDUĞU İÇİN TERSİ YOKTUR");
                System.Environment.Exit(0);
            }

            GösterAraMatris araMat = new GösterAraMatris();
            this.Hide();
            araMat.ShowDialog();

           
 }

        public double[,] CarpımTranspoze(double[,] matris, double[,] matrisTranspoze)
        {
            if (Secim.t1 > Secim.t2)
            {
                for (int k = 0; k < Secim.t2; k++)
                {
                    for (int i = 0; i < Secim.t2; i++)
                    {

                        for (int j = 0; j < Secim.t1; j++)

                            carpımSonuc2[k, i] += matrisTranspoze[k, j] * matris[j, i];
                        
                    }
                }
            }
            else
            {
                for (int k = 0; k < Secim.t1; k++)
                {
                    for (int i = 0; i < Secim.t1; i++)
                    {

                        for (int j = 0; j < Secim.t2; j++)

                            carpımSonuc[k, i] += matris[k, j] * matrisTranspoze[j, i];
                    }
                }


            }
            
          //  MessageBox.Show(carpim.ToString() + " =carpim" + toplam.ToString() + " =toplam");
                return carpımSonuc;
            
        }
        public double[,] MatrisCarpim(double[,] mtrs,double[,] mtrs2,int b1,int b2,int b3)  //Carpım matrisinin boyutu girilen matrisin transpozu olmalıdır.    
        {
            
   
                for (int k = 0; k < b1; k++)
                {
                    for (int i = 0; i < b2; i++)
                    {

                        for (int j = 0; j < b3; j++)

                            SonCarpım[k, i] += mtrs[k, j] * mtrs2[j, i];
                       
                    }
                }

          /*  foreach (var item in SonCarpım)
            {
                MessageBox.Show(item.ToString()+"carpımm");
            }*/
            return SonCarpım;
            
        }
        public void ekMatris(double[,] matris, int n)
        {
            // MessageBox.Show(n.ToString() + "nn");
            if (n == 1)
            {
                TersMatrisDeğer[0,0] = (1.0 / matris[0, 0]);
               // MessageBox.Show("girdiii");
            }
            else
            {
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < n; j++)
                    {
                        getKofaktör(i, j, n, matris);
                    }
                }
            }
        }
        public void getKofaktör(int x, int y, int boyut, double[,] matris)
        {
            double[,] tmp = new double[boyut - 1, boyut - 1];
            int sat = 0, sut = 0;
            for (int i = 0; i < boyut; i++)
            {
                for (int j = 0; j < boyut; j++)
                {
                    if (i != x && j != y)
                    {
                        double z = matris[i, j];
                               //  MessageBox.Show(z.ToString()+"<-");
                        arrayMin.Add(z);

                        if (arrayMin.Count == (boyut - 1) * (boyut - 1))
                        {
                            minör.Add(determinantAl(matriscevir2(arrayMin, boyut - 1), boyut - 1));
                            Kofaktör.Add(Math.Pow(-1, (x + y + 2)) * determinantAl(matriscevir2(arrayMin,boyut-1), boyut - 1));

                          //  getDetToplamCarpim(boyut - 1); // determinant ıslem sayıları
                          //  MessageBox.Show(MatrisEkle.topDet.ToString() + "toplamm");

                            //  MessageBox.Show((boyut - 1).ToString() + "boyut-1");
                            arrayMin.Clear();

                        }
                    }
                }
                sat++;
                sut = 0;
            }
           
        }
        public double[,] getTersMatris(ArrayList list, int boyut, double[,] matris)
        {
            // MessageBox.Show(boyut.ToString() + "boyutt");
            TersMatris = new double[boyut, boyut];
            double[,] tutMatris = new double[boyut, boyut];
            double[,] tutMatrisT = new double[boyut, boyut];
            int t = 0;
            if (boyut == 1)
            {
                TersMatris[0, 0] = 1 / matris[0, 0];
                return TersMatris;
            }
            else
            {
                for (int i = 0; i < boyut; i++)
                {
                    for (int j = 0; j < boyut; j++)
                    {
                        //MessageBox.Show(list[0].ToString()+"listee");
                        tutMatris[i, j] = Convert.ToDouble(list[t++].ToString());


                    }
                }


                double detTut = 1 / determinantAl(matris, boyut);

                for (int i = 0; i < boyut; i++)
                {
                    for (int j = 0; j < boyut; j++)
                    {
                        tutMatrisT[i, j] = tutMatris[j, i]; // satır ve sutunları yer değiştir

                    }
                }

             
                for (int i = 0; i < boyut; i++)
                {
                    for (int j = 0; j < boyut; j++)
                    {
                        TersMatris[i, j] = detTut * tutMatrisT[i, j];//1/detA*ekA
                                                                     // MessageBox.Show(tutMatrisT[i, j].ToString() + "tutmatrissTT");
                                                                     // MessageBox.Show(TersMatris[i, j].ToString() + "ters matris");
                    }
                }

                return TersMatris;
            }
        }

         public double[,] Transpoze(double[,] matris)
        {
            
            
            for (int i = 0; i < Secim.t1; i++)
            {
                for (int j = 0; j <Secim.t2; j++)
                {
                    //MessageBox.Show(matris[i, j].ToString() + "matriss");
                    transpoze[j, i] = matris[i, j]; // satır ve sutunları yer değiştir
                    

                }
            }
            return transpoze;
        }
        public double determinantAl(double[,] matris, int n)
        {

            int sat, sut;
            double[,] tmp = new double[n, n];
            determinant = 0;

            if (n == 1)
            {
                return matris[0, 0];
            }

            else if (n == 2)
            {
                determinant = (matris[0, 0] * matris[1, 1] - matris[0, 1] * matris[1, 0]);

                return determinant;
            }

            else
            {
                for (int k = 0; k < n; k++)
                {
                    sat = 0;
                    sut = 0;

                    for (int i = 1; i < n; i++)
                    {
                        for (int j = 0; j < n; j++)
                        {
                           

                            if (j != k)
                            {
                                tmp[sat, sut] = matris[i, j];
                                sut++;
                            }
                            
                        }
                        sat++;
                        sut = 0;
                    }

                    determinant += Convert.ToDouble(matris[0, k] * Math.Pow(-1, k) * determinantAl(tmp, n - 1));
                }
                return determinant;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {/*
            Random random = new Random();
            foreach (TextBox textBox in MatrisEkle.textBoxes)
            {
                if (textBox is TextBox)
                {
                    ((TextBox)textBox).Text = Math.Round((random.NextDouble() * 10), 1).ToString();

                }

            }*/
        }

       
    }
}
