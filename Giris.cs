﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TERSMATRİS
{
    public partial class Giris : Form
    {
        public Giris()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Random random = new Random();
            int s1 = random.Next(1, 5);
            int s2 = random.Next(1, 5);

            if (s1 == s2)
            {
                s1 = random.Next(1, 5);
                s2 = random.Next(1, 5);
            }
            else
            {
                Secim sc = new Secim(s1, s2);
                MatrisEkle mt = new MatrisEkle(s1, s2);
                mt.Show();
                this.Hide();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Secim sec = new Secim();
            sec.Show();
            this.Hide();
        }
    }
}
