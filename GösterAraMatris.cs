﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TERSMATRİS
{
    public partial class GösterAraMatris : Form
    {
        MatrisEkle mEkle = new MatrisEkle();

        public static double n = Math.Sqrt(Convert.ToDouble(MatrisEkle.Kofaktör.Count));
        public static int topDet = 0;
        public static int carpDet = 0;
        public static int toplam = 0;//tranpoz ile çarpım
        public static int carpim = 0;
        ArrayList Toplam = new ArrayList();
        ArrayList Carpim = new ArrayList();

        static int sayac1 = 0;
        static int sayac2 = 0;
        static int sayac3 = 0;
        static int sayac4 = 0;
        static int sayac5 = 0;
        static int sayac6 = 0;

        public  double[,] sonuc = new double[Convert.ToInt32(n),Convert.ToInt64(n)];
        public GösterAraMatris()
        {
            InitializeComponent();
        }
        public void göster(double[,] matris)
        {
            int sat=matris.GetLength(0);
            int sut= matris.GetLength(1);
            dataGridView1.ColumnCount = sut;
            
           // MessageBox.Show(sat.ToString() + "***" + sut.ToString());
            dataGridView1.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            for (int i = 0; i < sat; i++)
            {
                DataGridViewRow row = new DataGridViewRow();
                row.CreateCells(this.dataGridView1);
                 for(int j = 0; j < sut; j++)
                {
                    // dataGridView1.Rows.Add(matris[i,j]);
                    row.Cells[j].Value = matris[i, j];
                    
                }
                this.dataGridView1.Rows.Add(row);
            }

        }
        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            göster(MatrisEkle.transpoze);
            textBox1.Text = 0.ToString();
            textBox2.Text = 0.ToString();


        }

        private void button2_Click(object sender, EventArgs e)
        {

            if (Secim.t1 > Secim.t2)
            {
                dataGridView1.Rows.Clear();
                göster(MatrisEkle.carpımSonuc2);
                carpim = Secim.t2 * Secim.t2 * Secim.t1;
                toplam = Secim.t2* Secim.t2 * (Secim.t1 - 1);
            }

            else
            {
                dataGridView1.Rows.Clear();
                göster(MatrisEkle.carpımSonuc);
                carpim = Secim.t1 * Secim.t1 * Secim.t2;
                toplam = Secim.t1 * Secim.t1 * (Secim.t2 - 1);

            }
            if (sayac1 == 0)
            {
                Toplam.Add(toplam);
                Carpim.Add(carpim);
                sayac1++;
            }
            textBox1.Text = toplam.ToString();
            textBox2.Text = carpim.ToString();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            
          //  MessageBox.Show(MatrisEkle.to.ToString() + "toplamm");

            dataGridView1.Rows.Clear();

            double[,] mtrs = new double[1, 1];

            if (Secim.t1 == 1 || Secim.t2 == 1)
            {
                mtrs[0, 0] = 1;
                göster(mtrs);
            }
            else
            {
                göster(matriscevir2(MatrisEkle.minör, Convert.ToInt32(n)));
            }

            if (Secim.t1 > Secim.t2){
                getDetToplamCarpim(Secim.t2-1);
                textBox1.Text = ((topDet)*(Secim.t2*Secim.t2)).ToString();
                textBox2.Text = ((carpDet) * (Secim.t2 * Secim.t2)).ToString();
                if (sayac2 == 0)
                {
                    Toplam.Add(((topDet) * (Secim.t2 * Secim.t2)));
                    Carpim.Add(((carpDet) * (Secim.t2 * Secim.t2)));
                    sayac2++;
                }
            }

            else if (Secim.t2 > Secim.t1){
                getDetToplamCarpim(Secim.t1-1);
                textBox1.Text = ((topDet) * (Secim.t1 * Secim.t1)).ToString();
                textBox2.Text = ((carpDet) * (Secim.t1 * Secim.t1)).ToString();
                if (sayac2 == 0)
                {
                    Toplam.Add(((topDet) * (Secim.t1 * Secim.t1)));
                    Carpim.Add(((carpDet) * (Secim.t1 * Secim.t1)));
                    sayac2++;
                }
            }
            
        }

        private void button4_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
             double[,] mtrs= new double[1, 1];
            
            if (Secim.t1==1 || Secim.t2 == 1)
            {
                mtrs[0, 0] = 1;
                göster(mtrs);
            }
            else
            {
                göster(transpozeAl(mEkle.matriscevir2(MatrisEkle.Kofaktör, Convert.ToInt32(n))));
            }
            textBox1.Text = 0.ToString();
            textBox2.Text = 0.ToString();



        }

        private void button5_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            göster(MatrisEkle.TersMatris);
            if (Secim.t1 > Secim.t2)
            {
                textBox1.Text = (Secim.t2 * Secim.t2).ToString();
                textBox2.Text = (Secim.t2 * Secim.t2).ToString();
                if (sayac4 == 0)
                {
                    Toplam.Add((Secim.t2 * Secim.t2));
                    Carpim.Add((Secim.t2 * Secim.t2));
                    sayac4++;
                }
                
            }
            else
            {
                textBox1.Text = (Secim.t1 * Secim.t1).ToString();
                textBox2.Text = (Secim.t1 * Secim.t1).ToString();
                if (sayac4 == 0)
                {
                    Toplam.Add((Secim.t1 * Secim.t1));
                    Carpim.Add((Secim.t1 * Secim.t1));
                    sayac4++;
                }
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            göster(MatrisEkle.SonCarpım);
            if (Secim.t1 > Secim.t2)
            {
                textBox1.Text = ((Secim.t2 - 1) * (Secim.t1)*(Secim.t2)).ToString();
                textBox2.Text = (Secim.t2 * Secim.t2 * Secim.t1).ToString();
                if (sayac5 == 0)
                {
                    Toplam.Add(((Secim.t2 - 1) * (Secim.t1) * (Secim.t2)));
                    Carpim.Add((Secim.t2 * Secim.t2 * Secim.t1));
                    sayac5++;
                }
            }
            else
            {
                textBox1.Text = ((Secim.t1 - 1) * (Secim.t1) * (Secim.t2)).ToString();
                textBox2.Text = (Secim.t2 * Secim.t1 * Secim.t1).ToString();
                if (sayac5 == 0)
                {
                    Toplam.Add(((Secim.t1 - 1) * (Secim.t1) * (Secim.t2)));
                    Carpim.Add((Secim.t2 * Secim.t1 * Secim.t1));
                    sayac5++;
                }
            }

        }

        private void button3_Click(object sender, EventArgs e)
        {
            
            dataGridView1.Rows.Clear();

            double[,] mtrs = new double[1, 1];

            if (Secim.t1 == 1 || Secim.t2 == 1)
            {
                mtrs[0, 0] = 1;
                göster(mtrs);
            }
            else
            {
                göster(matriscevir2(MatrisEkle.Kofaktör, Convert.ToInt32(n)));
            }
            if (Secim.t1 > Secim.t2)
            {
                textBox1.Text = (Secim.t2 * Secim.t2).ToString();
                textBox2.Text = (Secim.t2 * Secim.t2).ToString();
                if (sayac3 == 0)
                {
                    Toplam.Add((Secim.t2 * Secim.t2));
                    Carpim.Add((Secim.t2 * Secim.t2));
                    sayac3++;
                }
            }
            else
            {
                textBox1.Text = (Secim.t1 * Secim.t1).ToString();
                textBox2.Text = (Secim.t1 * Secim.t1).ToString();
                if (sayac3 == 0)
                {
                    Toplam.Add((Secim.t1 * Secim.t1));
                    Carpim.Add((Secim.t1 * Secim.t1));
                    sayac3++;
                }
            }
        }
        public double[,] matriscevir2(ArrayList array, int n)
        {
            //ArrayList tersarray = new ArrayList();
            int k = 0;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {

                    sonuc[i, j] = Convert.ToDouble(array[k++]);


                }
            }

            return sonuc;
        }
        public double[,] transpozeAl(double[,] matris)
        {
            double[,] transpoze = new double[Convert.ToInt32(n), Convert.ToInt32(n)];
            for (int i = 0; i < Convert.ToInt32(n); i++)
            {
                for (int j = 0; j < Convert.ToInt32(n); j++)
                {
                    transpoze[j, i] = matris[i, j]; // satır ve sutunları yer değiştir

                }
            }
            return transpoze;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void GösterAraMatris_Load(object sender, EventArgs e)
        {

        }
        public void getDetToplamCarpim(int n)
        {
           if (n == 1 || n==0)
            {
                topDet = 0;
                carpDet = 0;
            }
            if (n == 2 )
            {
                topDet = 1;
                carpDet = 2;
            }
            if (n == 3)
            {
                topDet = 8;
                carpDet = 12;
            }
            if (n == 4)
            {
                topDet = 39;
                carpDet = 56;
            }
            if (n == 5)
            {
                carpDet = 290;
                topDet = 204;
            }
          

        }

        private void button8_Click(object sender, EventArgs e)
        {
            int toplam = 0;
            int carpim = 0;
            foreach (var item in Toplam) {
                toplam += Int32.Parse(item.ToString());
            }
            foreach (var item in Carpim)
            {
                carpim += Int32.Parse(item.ToString());
            }
            textBox1.Text = toplam.ToString();
            textBox2.Text = carpim.ToString();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();
            double[,] matris = new double[1, 1];
           // MessageBox.Show(MatrisEkle.determinant.ToString());
            matris[0, 0] = MatrisEkle.determinant;
           
            göster(matris);
            if (Secim.t1 > Secim.t2)
            {
                getDetToplamCarpim(Secim.t2);
                textBox1.Text = topDet.ToString();
                textBox2.Text = carpDet.ToString();
                if (sayac6 == 0)
                {
                    Toplam.Add(topDet);
                    Carpim.Add(carpDet);
                    sayac6++;
                }
            }
            else
            {
                getDetToplamCarpim(Secim.t1);
                textBox1.Text = topDet.ToString();
                textBox2.Text = carpDet.ToString();
                if (sayac6 == 0)
                {
                    Toplam.Add(topDet);
                    Carpim.Add(carpDet);
                    sayac6++;
                }
            }
        }
    }
}
